let vimDir = '$HOME/.vim'
let &runtimepath.=','.vimDir

" Keep undo history across sessions by storing it in a file
if has('persistent_undo')
    let myUndoDir = expand(vimDir . '/undodir')
    " Create dirs
    call system('mkdir ' . vimDir)
    call system('mkdir ' . myUndoDir)
    let &undodir = myUndoDir
    set undofile
endif

execute pathogen#infect()
syntax on
set nocompatible
filetype plugin indent on

if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_cloudformation_checkers = ['cfn_lint']
let g:syntastic_cloudformation_cfn_lint_args = "--ignore-checks W3005 --"

let delimitMate_expand_cr = 1

let g:indentLine_char_list = ['|', '¦', '┆', '┊']

" yaml indentation
au FileType yaml setlocal tabstop=2 expandtab shiftwidth=2 softtabstop=2
" json indentation
au FileType json setlocal tabstop=2 expandtab shiftwidth=2 softtabstop=2
au FileType template setlocal tabstop=2 expandtab shiftwidth=2 softtabstop=2

command Jq %!jq '.'
command Fold setlocal foldmethod=indent
